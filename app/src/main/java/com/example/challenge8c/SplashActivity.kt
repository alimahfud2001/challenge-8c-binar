package com.example.challenge8c

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Observer
import com.example.challenge8c.manager.DataStoreManager
import com.example.challenge8c.model.GetMovieResponseItem
import com.example.challenge8c.viewmodel.MovieViewModel
import okhttp3.internal.wait
import org.koin.android.ext.android.inject

class MainActivity : ComponentActivity() {
    private val viewmodel: MovieViewModel by inject()
    private val datastore: DataStoreManager by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        datastore.setContext(this)
        setContent {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
                modifier = Modifier.fillMaxSize().background(color = Color.DarkGray)
            ) {
                SetImage(150)
            }
        }

        var isLoggedIn = ""
        viewmodel.getLoginStatus().observe(this){
            isLoggedIn = it
            }
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        Handler().postDelayed({
            when (isLoggedIn){
                "" -> startActivity(Intent(this, LoginActivity::class.java))
                else -> startActivity(Intent(this, HomeActivity::class.java))
            }
            finish()
        }, 2000)
    }
}

@Composable
fun SetImage(size:Int){
    val image = painterResource(R.drawable.ic_movie)
    Image(image, null, Modifier.size(size.dp, size.dp), Alignment.Center)

}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier.fillMaxSize().background(color = Color.DarkGray)
    ) {
        SetImage(150)
    }
}