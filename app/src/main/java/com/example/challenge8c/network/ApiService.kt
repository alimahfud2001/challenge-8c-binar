package com.example.challenge8c.network

import com.example.challenge8c.model.GetMovieResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

open interface ApiService {
    @GET("movie/top_rated")
    fun getTopRated(
        @Query("api_key") apiKey: String = "e587910f9f7819198c62e384c9fdfaaf"
    ): Call<GetMovieResponse>
}