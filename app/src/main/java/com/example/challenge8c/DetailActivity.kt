package com.example.challenge8c

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import coil.size.Scale
import coil.transform.CircleCropTransformation
import com.example.challenge8c.model.GetMovieResponseItem
import com.example.challenge8c.ui.theme.Challenge8CTheme

class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val data = intent.getParcelableExtra<GetMovieResponseItem>("movieData")
        setContent {
            Column(modifier = Modifier
                .background(Color.LightGray)
                .fillMaxSize()
                .padding(horizontal = 20.dp)) {
                Image(
                    painter = rememberImagePainter(
                        data = "https://image.tmdb.org/t/p/w342${data?.posterPath}",
                        builder = {
                            placeholder(R.drawable.ic_movie)
                        }
                    ),
                    contentDescription = null,
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .padding(top = 10.dp)
                        .fillMaxWidth(1f)
                )
                Text(text = "${data?.title}",
                    modifier = Modifier
                        .padding(top = 20.dp)
                        .align(Alignment.CenterHorizontally),
                    fontSize = 25.sp)
                Text(text = "Popularity: ${data?.popularity}",
                    modifier = Modifier
                        .padding(top = 20.dp)
                        .align(Alignment.Start),
                    fontSize = 15.sp)
                Text(text = "Release Date: ${data?.releaseDate}",
                    modifier = Modifier
                        .padding(top = 5.dp)
                        .align(Alignment.Start),
                    fontSize = 15.sp)
                Text(text = "Overview:",
                    modifier = Modifier
                        .padding(top = 5.dp)
                        .align(Alignment.Start),
                    fontSize = 15.sp)
                Text(text = "${data?.overview}",
                    modifier = Modifier
                        .padding(top = 5.dp)
                        .align(Alignment.Start),
                    fontSize = 15.sp)
            }
        }
    }
}

