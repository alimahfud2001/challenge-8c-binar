package com.example.challenge8c

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Movie
import android.graphics.drawable.Drawable
import android.icu.number.Scale
import android.os.Bundle
import android.util.Log
import android.view.Gravity.FILL
import android.view.View
import android.widget.GridLayout.FILL
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.Snapshot.Companion.observe
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.input.key.Key.Companion.F
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.Observer
import coil.compose.rememberImagePainter
import coil.transform.CircleCropTransformation
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.example.challenge8c.manager.DataStoreManager
import com.example.challenge8c.model.GetMovieResponse
import com.example.challenge8c.model.GetMovieResponseItem
import com.example.challenge8c.ui.theme.Challenge8CTheme
import com.example.challenge8c.viewmodel.MovieViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.OkHttpClient
import org.koin.android.ext.android.inject

class HomeActivity : ComponentActivity() {
    private val viewmodel : MovieViewModel by inject()
    private val datastore: DataStoreManager by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        datastore.setContext(this)
        setContent {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.LightGray)
            ) {
                Text(text = "Logout",
                    fontSize = 20.sp,
                    modifier = Modifier
                        .fillMaxWidth(0.95f)
                        .wrapContentWidth(Alignment.End)
                        .padding(top = 10.dp, bottom = 10.dp)
                        .clickable(onClick = {
                            viewmodel.saveLoginStatus("")
                            startActivity(Intent(this@HomeActivity, LoginActivity::class.java))
                            finishAffinity()
                        })
                )
                Surface(color = Color.LightGray) {
                    MovieList(movieList = viewmodel.movieList)
                    viewmodel.fetchAllData()}
            }
        }
    }
}

@Composable
fun MovieList(movieList: List<GetMovieResponseItem>) {
    var selectedIndex by remember { mutableStateOf(-1) }
    LazyColumn {
        itemsIndexed(items = movieList) { index, item ->
            MovieItem(movie = item, index, selectedIndex) { i ->
                selectedIndex = i
            }
        }
    }
}

@Composable
fun MovieItem(movie: GetMovieResponseItem, index: Int, selectedIndex: Int, onClick: (Int) -> Unit) {
    val context = LocalContext.current
    val backgroundColor =
        if (index == selectedIndex) MaterialTheme.colors.primary else MaterialTheme.colors.background
    Card(
        modifier = Modifier
            .padding(8.dp, 4.dp)
            .fillMaxWidth()
            .clickable (onClick = {
                index
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra("movieData", movie)
                context.startActivity(intent)
            })
            .height(110.dp), shape = RoundedCornerShape(8.dp), elevation = 4.dp
    ) {
        Surface() {

            Row(
                Modifier
                    .padding(4.dp)
                    .fillMaxSize()
            ) {

                Image(
                    painter = rememberImagePainter(
                        data = "https://image.tmdb.org/t/p/w342${movie.posterPath}",
                        builder = {
                            scale(coil.size.Scale.FILL)
                            placeholder(R.drawable.ic_movie)
                            transformations(CircleCropTransformation())
                        }
                    ),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxHeight()
                        .weight(0.2f)
                )


                Column(
                    verticalArrangement = Arrangement.Center,
                    modifier = Modifier
                        .padding(4.dp)
                        .fillMaxHeight()
                        .weight(0.8f)
                ) {
                    Text(
                        text = movie.title,
                        style = MaterialTheme.typography.subtitle1,
                        fontWeight = FontWeight.Bold
                    )
                    Text(
                        text = movie.popularity.toString(),
                        style = MaterialTheme.typography.caption,
                        modifier = Modifier
                            .background(
                                Color.LightGray
                            )
                            .padding(4.dp)
                    )
                    Text(
                        text = movie.overview,
                        style = MaterialTheme.typography.body1,
                        maxLines = 2,
                        overflow = TextOverflow.Ellipsis
                    )

                }
            }
        }
    }

}


