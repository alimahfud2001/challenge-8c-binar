package com.example.challenge8c.model

import com.google.gson.annotations.SerializedName

data class GetMovieResponse(
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val results: List<GetMovieResponseItem>,
    @SerializedName("total_pages")
    val totalPages: Int
)