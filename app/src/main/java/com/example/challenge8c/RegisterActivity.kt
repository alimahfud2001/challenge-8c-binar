package com.example.challenge8c

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContentProviderCompat.requireContext
import com.example.challenge8c.manager.DataStoreManager
import com.example.challenge8c.viewmodel.MovieViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.koin.android.ext.android.inject

class RegisterActivity : ComponentActivity() {
    private val viewmodel: MovieViewModel by inject()
    private val datastore: DataStoreManager by inject()
    private var data = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        datastore.setContext(this)
        setContent {
            var userData: ArrayList<String> = arrayListOf("")
            var username by remember {
                mutableStateOf("")
            }
            var email by remember {
                mutableStateOf("")
            }
            var password by remember {
                mutableStateOf("")
            }
            var confirmPassword by remember {
                mutableStateOf("")
            }
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.LightGray)
                    .padding(40.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(text = "Register", fontSize = 35.sp, color = Color.Black)
                Spacer(modifier = Modifier.padding(15.dp))
                Image(painterResource(R.drawable.ic_movie), null, modifier = Modifier.size(100.dp, 100.dp))
                Spacer(modifier = Modifier.padding(15.dp))
                TextField(
                    value = username,
                    label = {
                        Text(text = "Username")
                    },
                    onValueChange = {
                        username = it
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.DarkGray))
                Spacer(modifier = Modifier.padding(5.dp))
                TextField(
                    value = email,
                    label = {
                        Text(text = "Email")
                    },
                    onValueChange = {
                        email = it
                        observer(email)
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.DarkGray))
                Spacer(modifier = Modifier.padding(5.dp))
                TextField(
                    value = password,
                    label = {
                        Text(text = "Password")
                    },
                    onValueChange = {
                        password = it
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.DarkGray))
                Spacer(modifier = Modifier.padding(5.dp))
                TextField(
                    value = confirmPassword,
                    label = {
                        Text(text = "Confirm Password")
                    },
                    onValueChange = {
                        confirmPassword = it
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.DarkGray))
                Spacer(modifier = Modifier.padding(40.dp))
                Button(onClick ={
                    userData = arrayListOf(username, email, password)
                    val gson = Gson()
                    if (username == ""||email == ""||password == ""||confirmPassword == "")
                        Toast.makeText(this@RegisterActivity, "Isi Detail", Toast.LENGTH_SHORT).show()
                    else {
                        val set = gson.fromJson<ArrayList<String>>(data, object : TypeToken<ArrayList<String>>(){}.type)
                        if (set?.elementAt(1) != null) {
                            Toast.makeText(this@RegisterActivity, "Email Sudah Terdaftar", Toast.LENGTH_SHORT).show()
                        }
                        else {
                            if (password != confirmPassword) {
                                Toast.makeText(this@RegisterActivity, "Password tidak cocok", Toast.LENGTH_SHORT).show()
                            }
                            else {
                                viewmodel.saveDataStore(email, gson.toJson(userData))
                                Toast.makeText(this@RegisterActivity, "$userData", Toast.LENGTH_SHORT).show()
                                startActivity(Intent(this@RegisterActivity, LoginActivity::class.java))
                                finishAffinity()
                            }
                        }
                    }
                }, modifier = Modifier.size(100.dp, 40.dp), colors = ButtonDefaults.buttonColors(Color.DarkGray)) {
                    Text(text = "Register")
                }
            }
        }
    }

    private fun observer(email:String) {
        viewmodel.apply {
            getDataStore(email).observe(this@RegisterActivity) {
                data = it
            }
        }
    }
}