package com.example.challenge8c

import android.app.Application
import com.example.challenge8c.manager.DataStoreManager
import com.example.challenge8c.network.ApiService
import com.example.challenge8c.viewmodel.MovieViewModel
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(listOf(viewModelModule, dataStoreManagerModule, apiModule, retrofitModule))
        }
    }
}

val viewModelModule = module {
    single {
        MovieViewModel(get(), get())
    }
}

val dataStoreManagerModule = module {
    single {
        DataStoreManager()
    }
}

val apiModule = module {
    fun provideUseApi(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    single { provideUseApi(get()) }
}

val retrofitModule = module {
    fun provieHttpClient(): OkHttpClient {
        val client = OkHttpClient.Builder()
        return client.build()
    }

    fun provideRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    single { provieHttpClient() }
    single { provideRetrofit(get()) }
}
