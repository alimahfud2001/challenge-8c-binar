package com.example.challenge8c.manager

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class DataStoreManager() {
    private lateinit var context: Context

    suspend fun setData(email: String, data: String) {
        context.userDataStore.edit { preferences ->
            val key = stringPreferencesKey(email)
            preferences[key] = data
        }
    }

    suspend fun setLoginStatus(status: String) {
        context.userDataStore.edit { preferences ->
            val key = stringPreferencesKey("IsLoggedIn")
            preferences[key] = status
        }
    }

    fun setContext(context: Context) {
        this.context =context
    }

    fun getData(email: String): Flow<String> {
        return context.userDataStore.data.map { preferences ->
            val key = stringPreferencesKey(email)
            preferences[key] ?: "" }
    }

    fun getLoginStatus(): Flow<String> {
        return context.userDataStore.data.map { preferences ->
            val key = stringPreferencesKey("IsLoggedIn")
            preferences[key] ?: "" }
    }

    companion object {
        private const val DATASTORE_NAME = "user_preferences"
        private val Context.userDataStore by preferencesDataStore(
            name = DATASTORE_NAME
        )
    }
}