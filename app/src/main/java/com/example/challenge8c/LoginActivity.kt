package com.example.challenge8c

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContentProviderCompat.requireContext
import com.example.challenge8c.manager.DataStoreManager
import com.example.challenge8c.ui.theme.Challenge8CTheme
import com.example.challenge8c.viewmodel.MovieViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.koin.android.ext.android.inject

class LoginActivity : ComponentActivity() {
    private val viewmodel: MovieViewModel by inject()
    private val datastore: DataStoreManager by inject()
    private var dataJson = ""
    private val gson = Gson()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        datastore.setContext(this)
        setContent {
            var userData: ArrayList<String> = arrayListOf("")
            var email by remember {
                mutableStateOf("")
            }
            var password by remember {
                mutableStateOf("")
            }
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.LightGray)
                    .padding(40.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(text = "Login", fontSize = 40.sp, color = Color.Black)
                Spacer(modifier = Modifier.padding(15.dp))
                Image(painterResource(R.drawable.ic_movie), null, modifier = Modifier.size(100.dp, 100.dp))
                Spacer(modifier = Modifier.padding(25.dp))
                TextField(
                    value = email,
                    label = {
                        Text(text = "Email")
                    },
                    onValueChange = {
                        email = it
                        observer(email)
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.DarkGray))
                Spacer(modifier = Modifier.padding(5.dp))
                TextField(
                    value = password,
                    label = {
                        Text(text = "Password")
                    },
                    onValueChange = {
                        password = it
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.DarkGray))
                Spacer(modifier = Modifier.padding(60.dp))
                Button(onClick ={
                    userData = arrayListOf(email, password)
                    val set = gson.fromJson<ArrayList<String>>(dataJson, object : TypeToken<ArrayList<String>>(){}.type)
                    if (email == ""||password == "")
                        Toast.makeText(this@LoginActivity, "Isi Detail", Toast.LENGTH_SHORT).show()
                    else {
                        if (set?.elementAt(1) == null)
                            Toast.makeText(this@LoginActivity, "Email Tidak Terdaftar", Toast.LENGTH_SHORT).show()
                        else{
                            if (password != set.elementAt(2))
                                Toast.makeText(this@LoginActivity, "Email/Password Salah", Toast.LENGTH_SHORT).show()
                            else {
                                viewmodel.saveLoginStatus(email)
                                startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
                                finishAffinity()
                            }
                        }
                    }

                }, modifier = Modifier.size(100.dp, 40.dp), colors = ButtonDefaults.buttonColors(Color.DarkGray)) {
                    Text(text = "Login")
                }
                Text(text = "Register here", fontSize = 15.sp, modifier = Modifier
                    .padding(10.dp)
                    .clickable(onClick = {
                        startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
                    }))
            }
        }
    }

    private fun observer(email:String) {
        viewmodel.apply {
            getDataStore(email).observe(this@LoginActivity) {
                dataJson = it
            }
        }
    }
}